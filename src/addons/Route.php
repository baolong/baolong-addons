<?php

namespace think\addons;

use think\Container;
use think\exception\HttpException;
use think\facade\Config;
use think\facade\Hook;
use think\facade\Lang;
use think\facade\Request;
use think\facade\Session;
use think\Loader;
use think\facade\Log;
/**
 * 插件执行默认控制器
 * @package think\addons
 */
class Route
{

    /**
     * 插件执行
     */
    public function execute($addon = null,  $controller = null, $action = null)
    {
        $app =  Container::get('app');
        $request = $app->request;
        Hook::listen('addon_begin', $request);
        
        // 是否自动转换控制器和操作名
        $convert = Config::get('app.url_convert');
        $filter = $convert ? 'strtolower' : 'trim';

        $addon = $addon ? trim(call_user_func($filter, $addon)) : '';
        $controller = $controller ? trim(call_user_func($filter, $controller)) : 'index';
        $action = $action ? trim(call_user_func($filter, $action)) : 'index';

        if (!empty($addon) && !empty($controller) && !empty($action)) {
            // 获取插件基础信息
            $info = get_addon_info($addon);
            if (!$info) {
                throw new HttpException(404, __('addon %s not found', $addon));
            }
            if (!$info['state']) {
                throw new HttpException(500, __('addon %s is disabled', $addon));
            }

            // 设置当前请求的控制器、操作
            $request->addon = $addon;
            $request->setController($controller)->setAction($action);

            // 监听addon_module_init
            Hook::listen('addon_module_init', $request);


            $class = get_addon_class($addon, 'controller', $controller);
            if (!$class) {
                throw new HttpException(404, Loader::parseName($controller, 1) . " 插件控制器未找到");
            }

            $instance = new $class($app);
            $vars = [];
            if (is_callable([$instance, $action])) {
                // 执行操作方法
                $call = [$instance, $action];
            } elseif (is_callable([$instance, '_empty'])) {
                // 空操作
                $call = [$instance, '_empty'];
                $vars = [$action];
            } else {
                // 操作不存在
                throw new HttpException(404, get_class($instance) . '->' . $action . '() 插件控制器方法未找到' );
            }
            $call = (object)$call;
            Hook::listen('addon_action_begin', $call);
            $call = (array)$call;
            return call_user_func_array($call, $vars);
        } else {
            abort(500, lang('addon can not be empty'));
        }
    }

}